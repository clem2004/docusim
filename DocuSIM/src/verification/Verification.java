/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package verification;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.github.javaparser.ParseProblemException;
import com.github.javaparser.Position;
import com.github.javaparser.Range;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.CallableDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.VariableDeclarationExpr;
import com.github.javaparser.ast.nodeTypes.NodeWithJavadoc;
import com.github.javaparser.javadoc.Javadoc;
import com.github.javaparser.javadoc.JavadocBlockTag;
import com.github.javaparser.javadoc.description.JavadocDescriptionElement;
import com.github.javaparser.javadoc.description.JavadocInlineTag;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.MemoryTypeSolver;

/**
 * Représente la vérification d'un fichier.
 * 
 * @author Jean-Félix Morency
 */
public class Verification {

	/** Le fichier. */
	private File fichier;
	/** Le code du fichier. */
	private String code;
	/** L'unité de compilation. */
	private CompilationUnit uniteCompilation;
	/** Les problèmes de documentation dans le fichier. */
	private Set<Probleme> problemes = new HashSet<>();
	/** Les commentaires d'auteur des méthodes. */
	private Map<CallableDeclaration<?>, List<Comment>> commentairesAuteur = new HashMap<>();
	/** Les auteurs des classes, méthodes et énumérations. */
	private Map<Node, Set<String>> auteurs = new HashMap<>();

	/**
	 * Vérifie un fichier.
	 * 
	 * @param fichier le fichier.
	 * @throws IOException s'il y a une erreur lors de la lecture du fichier.
	 */
	public Verification(File fichier) throws IOException {
		this.fichier = fichier;
		code = new String(Files.readAllBytes(fichier.toPath()), StandardCharsets.UTF_8);

		try {
			StaticJavaParser.getParserConfiguration().setSymbolResolver(new JavaSymbolSolver(new MemoryTypeSolver()));
			uniteCompilation = StaticJavaParser.parse(code);
		} catch (ParseProblemException e) {
			uniteCompilation = new CompilationUnit();
			code = "Le fichier n'a pas pu être analysé car il contient de la syntaxe non supportée :\n";
			problemes.add(new Probleme(new Range(new Position(1, 0), new Position(1, 0)), Erreur.SYNTAXE_INVALIDE));
			e.getProblems().forEach(probleme -> {
				code += "\n" + probleme.getVerboseMessage() + "\n";
				probleme.getLocation().ifPresent(jetons -> code += jetons + "\n");
			});
		}

		trouverAuteursMethodes();
		uniteCompilation.findAll(ClassOrInterfaceDeclaration.class).forEach(cd -> trouverAuteurs(cd));
		uniteCompilation.findAll(EnumDeclaration.class).forEach(ed -> trouverAuteurs(ed));
		detecterProblemes();
	}

	/**
	 * Retourne les auteurs trouvés dans le fichier.
	 * 
	 * @return les auteurs trouvés dans le fichier.
	 */
	public Set<String> getAuteurs() {
		Set<String> auteurs = new HashSet<>();
		this.auteurs.values().forEach(noms -> auteurs.addAll(noms));
		return auteurs;
	}

	/**
	 * Retourne la plus haute sévérité des problèmes du fichier.
	 * 
	 * @return la plus haute sévérité des problèmes du fichier.
	 */
	public Severite getSeverite() {
		if (problemes.size() == 0) {
			return null;
		}
		List<Severite> severites = problemes.stream().map(prob -> prob.getSeverite()).toList();
		return Severite.maximale(severites);
	}

	/**
	 * Trouve les auteurs des éléments à partir des commentaires.
	 */
	private void trouverAuteursMethodes() {
		// Liste des noeuds
		List<Node> noeuds = new ArrayList<>() {
			/** Identifiant de sérialisation. */
			private static final long serialVersionUID = 1L;

			/** {@inheritDoc} */
			@Override
			public int indexOf(Object o) {
				for (int i = 0; i < size(); i++)
					if (get(i) == o)
						return i;
				return -1;
			}
		};

		uniteCompilation.walk(node -> node.getBegin().ifPresent(debut -> noeuds.add(node)));
		uniteCompilation.getAllComments().forEach(comment -> {
			if (!comment.isOrphan())
				noeuds.add(comment);
		});
		noeuds.sort((Node a, Node b) -> a.getBegin().get().compareTo(b.getBegin().get()));

		// Détecter les commentaires
		for (int i = 0; i < noeuds.size() - 1; i++) {
			if (noeuds.get(i) instanceof Comment) {
				Comment commentaire = (Comment) noeuds.get(i);
				if (commentaire.isOrphan() && !(noeuds.get(i + 1) instanceof Comment)) {
					noeuds.get(i + 1).setComment(commentaire);
				}
			}
		}

		// Déterminer les commentaires d'auteur sur chaque méthode
		uniteCompilation.findAll(CallableDeclaration.class).forEach(cd -> {
			List<Comment> commentaires = new ArrayList<>();
			Set<String> auteursMethode = new HashSet<>();

			Comment commentaireMethode = cd.getComment().orElse(null);
			if (commentaireMethode != null && !commentaireMethode.isJavadocComment()) {
				cd.getParentNode().ifPresent(noeud -> noeud.addOrphanComment(commentaireMethode));

				int i = noeuds.indexOf(commentaireMethode);
				while (i >= 0 && noeuds.get(i) instanceof LineComment) {
					LineComment commentaire = (LineComment) noeuds.get(i);
					String contenu = commentaire.getContent().trim().toLowerCase();
					if (contenu.equals("fin") || contenu.startsWith("fin "))
						break;

					String auteur = commentaire.getContent().trim();
					if (contenu.startsWith("par "))
						auteur = auteur.substring(4).trim();
					auteursMethode.add(auteur);
					commentaires.add(0, commentaire);
					i--;
				}

				if (i >= 0 && noeuds.get(i) instanceof JavadocComment) {
					cd.setJavadocComment((JavadocComment) noeuds.get(i));
				} else {
					cd.setComment(null);
				}
			}

			commentairesAuteur.put(cd, commentaires);
			auteurs.put(cd, auteursMethode);
		});
	}

	/**
	 * Trouve les auteurs à partir de la Javadoc.
	 * 
	 * @param nj un noeud avec de la Javadoc.
	 */
	private void trouverAuteurs(NodeWithJavadoc<?> nj) {
		Set<String> auteursElement = new HashSet<>();
		nj.getJavadoc().ifPresent(javadoc -> javadoc.getBlockTags().forEach(tag -> {
			if (tag.getTagName().equals("author")) {
				auteursElement.add(tag.getContent().toText());
			}
		}));
		auteurs.put((Node) nj, auteursElement);
	}

	/**
	 * Trouve les problèmes de documentation dans le fichier.
	 */
	private void detecterProblemes() {
		uniteCompilation.findAll(ClassOrInterfaceDeclaration.class)
				.forEach(cd -> essayer(cd.getNameAsExpression(), "une classe", () -> verifierClasse(cd)));
		uniteCompilation.findAll(EnumDeclaration.class)
				.forEach(ed -> essayer(ed.getNameAsExpression(), "une énumération", () -> verifierEnum(ed)));
		uniteCompilation.findAll(CallableDeclaration.class)
				.forEach(md -> essayer(md, "une méthode", () -> verifierMethode(md)));
		uniteCompilation.findAll(FieldDeclaration.class)
				.forEach(fd -> essayer(fd, "une propriété", () -> verifierPropriete(fd)));
		uniteCompilation.findAll(VariableDeclarationExpr.class)
				.forEach(vde -> essayer(vde, "une variable", () -> verifierVariable(vde)));
		uniteCompilation.findAll(AnnotationExpr.class)
				.forEach(ae -> essayer(ae, "une annotation", () -> verifierAnnotation(ae)));
		uniteCompilation.getAllContainedComments()
				.forEach(c -> essayer(c, "un commentaire", () -> verifierCommentaire(c)));
	}

	/**
	 * Essaye de vérifier un élément et ajoute un avertissement si l'analyse échoue.
	 * 
	 * @param noeud    le noeud à vérifier.
	 * @param type     le type d'élément.
	 * @param verifier la méthode qui vérifie.
	 */
	private void essayer(Node noeud, String type, Runnable verifier) {
		try {
			verifier.run();
		} catch (Exception e) {
			problemes.add(new Probleme(noeud, Erreur.ECHEC_ANALYSE.avecParametre(type)));
			System.err.println("Échec de l'analyse d'un élément :");
			e.printStackTrace();
		}
	}

	/**
	 * Vérifie une propriété.
	 * 
	 * @param fd la propriété.
	 */
	private void verifierPropriete(FieldDeclaration fd) {
		@SuppressWarnings("unchecked")
		boolean pasJavadoc = fd.findAncestor(CallableDeclaration.class).isEmpty()
				&& (!fd.hasJavaDocComment() || fd.getJavadoc().get().getDescription().isEmpty());
		fd.getVariables().forEach(vd -> {
			List<Erreur> erreurs = new ArrayList<>();
			if (pasJavadoc) {
				erreurs.add(Erreur.PROPRIETE_SANS_JAVADOC);
			}
			if (fd.isPublic()) {
				erreurs.add(Erreur.PROPRIETE_PUBLIQUE);
			} else if (fd.isProtected()) {
				erreurs.add(Erreur.PROPRIETE_PROTEGE);
			} else if (!fd.isPrivate()) {
				erreurs.add(Erreur.PROPRIETE_VISIBILITE_DEFAUT);
			}

			if (fd.isFinal()) {
				boolean serialVersionUID = fd.isStatic() && vd.getTypeAsString().equals("long")
						&& vd.getNameAsString().equals("serialVersionUID");
				if (!serialVersionUID && !nomConstanteValide(vd.getNameAsString())) {
					erreurs.add(Erreur.IDENTIFICATEUR_CONSTANTE);
				}
			} else {
				if (!nomVariableValide(vd.getNameAsString())) {
					erreurs.add(Erreur.IDENTIFICATEUR_VARIABLE.avecParametre("propriété"));
				}
				if (fd.isStatic()) {
					erreurs.add(Erreur.PROPRIETE_STATIQUE);
				}
			}
			if (erreurs.size() > 0) {
				problemes.add(new Probleme(vd.getNameAsExpression(), erreurs));
			}
		});
	}

	/**
	 * Vérifie une variable.
	 * 
	 * @param vde la variable.
	 */
	private void verifierVariable(VariableDeclarationExpr vde) {
		vde.getVariables().forEach(vd -> {
			if (!nomVariableValide(vd.getNameAsString()))
				problemes.add(new Probleme(vd.getNameAsExpression(),
						Erreur.IDENTIFICATEUR_VARIABLE.avecParametre("variable")));
		});
	}

	/**
	 * Vérifie un commentaire.
	 * 
	 * @param c le commentaire.
	 */
	private void verifierCommentaire(Comment c) {
		List<Erreur> erreurs = new ArrayList<>();

		// Marque de conflit Git
		for (String ligne : c.getContent().split("\n\r?")) {
			if (ligne.startsWith(">>>>>>>") || ligne.startsWith("=======") || ligne.startsWith("<<<<<<<")) {
				erreurs.add(Erreur.CONFLIT_GIT.avecParametre(ligne.substring(0, 7)));
			}
		}

		// Commentaires Javadoc dans le vide
		if ((c.isJavadocComment() && c.isOrphan()) || (c.isBlockComment() && c.asString().startsWith("/**"))) {
			erreurs.add(Erreur.JAVADOC_FLOTTANTE);
		}

		if (!c.isJavadocComment()) {
			// Balise @author
			if (c.getContent().contains("@author")) {
				erreurs.add(Erreur.AUTHOR_NON_JAVADOC);
			}

			// Vérification des commentaires de noms
			boolean estAuteur = false;
			for (Set<String> noms : auteurs.values()) {
				for (String nom : noms) {
					if (c.getContent().toLowerCase().contains(nom.toLowerCase())) {
						estAuteur = true;
						break;
					}
				}
				if (estAuteur)
					break;
			}
			if (!estAuteur)
				return;

			for (List<Comment> commentaires : commentairesAuteur.values()) {
				for (Comment commentaire : commentaires) {
					if (commentaire == c)
						return;
				}
			}

			@SuppressWarnings("unchecked")
			boolean dansMethode = c.findAncestor(CallableDeclaration.class).isPresent();
			erreurs.add(dansMethode ? Erreur.NOM_DANS_METHODE : Erreur.NOM_MAL_PLACE);
		}

		if (erreurs.size() > 0) {
			problemes.add(new Probleme(c, erreurs));
		}
	}

	/**
	 * Vérifie une annotation.
	 * 
	 * @param ae l'annotation.
	 */
	private void verifierAnnotation(AnnotationExpr ae) {
		if (ae.getNameAsString().equals("SuppressWarnings")) {
			problemes.add(new Probleme(ae, Erreur.SUPPRESS_WARNINGS));
		}
	}

	/**
	 * Vérifie une classe.
	 * 
	 * @param cd la classe.
	 */
	private void verifierClasse(ClassOrInterfaceDeclaration cd) {
		List<Erreur> erreurs = new ArrayList<>();

		// Nom
		if (!nomClasseValide(cd.getNameAsString())) {
			erreurs.add(Erreur.IDENTIFICATEUR_CLASSE.avecParametre("classe"));
		}

		// Javadoc
		if (cd.hasJavaDocComment()) {
			Javadoc javadoc = cd.getJavadoc().get();
			List<Erreur> erreursJavadoc = new ArrayList<>();

			if (javadoc.getDescription().isEmpty()) {
				erreursJavadoc.add(Erreur.JAVADOC_VIDE);
			}

			// Auteurs
			@SuppressWarnings("unchecked")
			ClassOrInterfaceDeclaration parent = cd.findAncestor(ClassOrInterfaceDeclaration.class).orElse(null);
			if (auteurs.get(cd).size() == 0) {
				erreursJavadoc.add(Erreur.BALISE_AUTHOR_MANQUANTE);
			} else {
				for (String auteur : auteurs.get(cd)) {
					if (parent != null && !auteurs.get(parent).contains(auteur)) {
						erreursJavadoc.add(Erreur.AUTHOR_PAS_DANS_PARENT.avecParametre(auteur));
					}
					if (auteurs.get(cd).size() > 1) {
						boolean trouve = false;
						for (MethodDeclaration md : cd.getMethods()) {
							if (auteurs.get(md).contains(auteur)) {
								trouve = true;
								break;
							}
						}
						if (!trouve) {
							erreursJavadoc.add(Erreur.AUTHOR_SANS_METHODES.avecParametre(auteur));
						}
					}
				}
			}

			// Paramètres de type
			List<String> params = new ArrayList<>();
			javadoc.getBlockTags().forEach(balise -> {
				if (balise.getTagName().equals("param") && !balise.getContent().isEmpty())
					balise.getName().ifPresent(param -> params.add(param));
			});
			cd.getTypeParameters().forEach(param -> {
				String nom = param.getNameAsString();
				if (!params.remove("<" + nom + ">"))
					problemes.add(new Probleme(param, Erreur.PARAMETRE_TYPE_SANS_COMMENTAIRE.avecParametre(nom)));
			});
			params.forEach(param -> erreursJavadoc.add(Erreur.PARAMETRE_TYPE_INEXISTANT.avecParametre(param)));

			if (erreursJavadoc.size() > 0) {
				problemes.add(new Probleme(cd.getJavadocComment().get(), erreursJavadoc));
			}
		} else {
			erreurs.add(Erreur.ELEMENT_SANS_JAVADOC.avecParametre("classe"));
		}

		if (erreurs.size() > 0) {
			problemes.add(new Probleme(cd.getNameAsExpression(), erreurs));
		}
	}

	/**
	 * Vérifie une énumération.
	 * 
	 * @param ed l'énumération.
	 */
	private void verifierEnum(EnumDeclaration ed) {
		List<Erreur> erreurs = new ArrayList<>();

		// Nom
		if (!nomClasseValide(ed.getNameAsString())) {
			erreurs.add(Erreur.IDENTIFICATEUR_CLASSE.avecParametre("énumération"));
		}

		// Javadoc
		if (ed.hasJavaDocComment()) {
			Javadoc javadoc = ed.getJavadoc().get();
			List<Erreur> erreursJavadoc = new ArrayList<>();

			if (javadoc.getDescription().isEmpty()) {
				erreursJavadoc.add(Erreur.JAVADOC_VIDE);
			}

			if (auteurs.get(ed).size() == 0) {
				erreursJavadoc.add(Erreur.BALISE_AUTHOR_MANQUANTE);
			} else if (auteurs.get(ed).size() > 1) {
				erreursJavadoc.add(Erreur.PLUSIEURS_AUTHOR_ENUM);
			} else {
				@SuppressWarnings("unchecked")
				ClassOrInterfaceDeclaration parent = ed.findAncestor(ClassOrInterfaceDeclaration.class).orElse(null);
				String auteur = auteurs.get(ed).iterator().next();
				if (parent != null && !auteurs.get(parent).contains(auteur)) {
					erreursJavadoc.add(Erreur.AUTHOR_PAS_DANS_PARENT.avecParametre(auteur));
				}
			}

			if (erreursJavadoc.size() > 0) {
				problemes.add(new Probleme(ed.getJavadocComment().get(), erreursJavadoc));
			}
		} else {
			erreurs.add(Erreur.ELEMENT_SANS_JAVADOC.avecParametre("énumération"));
		}

		if (erreurs.size() > 0) {
			problemes.add(new Probleme(ed.getNameAsExpression(), erreurs));
		}

		// Items
		ed.getEntries().forEach(ecd -> {
			List<Erreur> erreursConstanteEnum = new ArrayList<>();

			if (!nomConstanteValide(ecd.getNameAsString()))
				erreursConstanteEnum.add(Erreur.IDENTIFICATEUR_CONSTANTE);
			if (!ecd.hasJavaDocComment() || ecd.getJavadoc().get().getDescription().isEmpty())
				erreursConstanteEnum.add(Erreur.ELEMENT_SANS_JAVADOC.avecParametre("constante d'énumération"));

			if (erreursConstanteEnum.size() > 0) {
				problemes.add(new Probleme(ecd, erreursConstanteEnum));
			}
		});
	}

	/**
	 * Vérifie une méthode.
	 * 
	 * @param cd la méthode.
	 */
	private void verifierMethode(CallableDeclaration<?> cd) {
		List<Erreur> erreurs = new ArrayList<>();

		// Nom de la méthode
		if (cd.isConstructorDeclaration() ? !nomClasseValide(cd.getNameAsString())
				: !nomVariableValide(cd.getNameAsString())) {
			erreurs.add(Erreur.IDENTIFICATEUR_VARIABLE.avecParametre("méthode"));
		}

		// Visibilité
		if (!cd.isPrivate() && !cd.isProtected() && !cd.isPublic()) {
			erreurs.add(Erreur.METHODE_VISIBILITE_DEFAUT);
		}

		// Auteurs
		Node parent = cd.getParentNode().orElse(null);
		while (parent != null
				&& !(parent instanceof CallableDeclaration || parent instanceof ClassOrInterfaceDeclaration)) {
			parent = parent.getParentNode().orElse(null);
		}
		Set<String> auteursParents = auteurs.getOrDefault(parent, new HashSet<>());

		if (commentairesAuteur.get(cd).size() > 1) {
			for (Comment comment : commentairesAuteur.get(cd)) {
				problemes.add(new Probleme(comment, Erreur.METHODE_PLUSIEURS_NOMS));
			}
		} else if (auteurs.get(cd).size() == 0) {
			if (auteursParents.size() > 1)
				erreurs.add(Erreur.METHODE_SANS_NOM);
		} else if (!auteursParents.contains(auteurs.get(cd).iterator().next())) {
			problemes.add(new Probleme(commentairesAuteur.get(cd).get(0),
					Erreur.NOM_PAS_AUTHOR.avecParametre(auteurs.get(cd).iterator().next())));
		}

		// Javadoc
		if (cd.hasJavaDocComment()) {
			Javadoc javadoc = cd.getJavadoc().get();
			List<Erreur> erreursJavadoc = new ArrayList<>();

			// Description
			if (javadoc.getDescription().isEmpty()) {
				erreursJavadoc.add(Erreur.JAVADOC_VIDE);
			}

			// {@inheritDoc}
			boolean inheritDoc = false;
			for (JavadocDescriptionElement element : javadoc.getDescription().getElements()) {
				if (element instanceof JavadocInlineTag
						&& ((JavadocInlineTag) element).getName().equals("inheritDoc")) {
					inheritDoc = true;
					break;
				}
			}
			if (inheritDoc && !cd.isAnnotationPresent("Override")) {
				erreursJavadoc.add(Erreur.INHERITDOC_SANS_OVERRIDE);
			}

			if (!inheritDoc) {
				// Lire les balises Javadoc
				List<String> params = new LinkedList<>();
				List<String> exceptions = new LinkedList<>();
				boolean returns = false;

				for (JavadocBlockTag balise : javadoc.getBlockTags()) {
					if (balise.getTagName().equals("author")) {
						erreursJavadoc.add(Erreur.AUTHOR_SUR_METHODE);
					} else if (!balise.getContent().isEmpty()) {
						switch (balise.getTagName()) {
							case "param":
								balise.getName().ifPresent(param -> params.add(param));
								break;
							case "throws":
								balise.getName().ifPresent(exception -> exceptions.add(exception));
								break;
							case "return":
								returns = true;
								break;
						}
					}
				}

				// @return
				if (cd.isMethodDeclaration()) {
					MethodDeclaration md = (MethodDeclaration) cd;
					if (md.getType().isVoidType() && returns) {
						erreursJavadoc.add(Erreur.BALISE_RETURN_VOID);
					} else if (!md.getType().isVoidType() && !returns) {
						problemes.add(new Probleme(md.getType(), Erreur.RETURN_MANQUANT));
					}
				} else if (returns) {
					erreursJavadoc.add(Erreur.BALISE_RETURN_CONSTRUCTEUR);
				}

				// @param <T>
				cd.getTypeParameters().forEach(paramType -> {
					String nom = paramType.getNameAsString();
					if (!params.remove("<" + nom + ">")) {
						problemes.add(
								new Probleme(paramType, Erreur.PARAMETRE_TYPE_SANS_COMMENTAIRE.avecParametre(nom)));
					}
				});

				// @param
				cd.getParameters().forEach(param -> {
					String nom = param.getNameAsString();
					if (!params.remove(param.getNameAsString())) {
						problemes.add(new Probleme(param, Erreur.PARAMETRE_SANS_COMMENTAIRE.avecParametre(nom)));
					}
				});

				// @throws
				cd.getThrownExceptions().forEach(exception -> {
					if (!exceptions.remove(exception.asString())) {
						problemes.add(new Probleme(exception,
								Erreur.EXCEPTION_SANS_COMMENTAIRE.avecParametre(exception.asString())));
					}
				});

				// Balises inutilisées
				for (String param : params) {
					erreursJavadoc.add(Erreur.PARAMETRE_INEXISTANT.avecParametre(param));
				}
				for (String exception : exceptions) {
					erreursJavadoc.add(Erreur.EXCEPTION_INEXISTANTE.avecParametre(exception));
				}
			}

			// Ajouter les erreurs de Javadoc
			if (erreursJavadoc.size() > 0) {
				problemes.add(new Probleme(cd.getJavadocComment().get(), erreursJavadoc));
			}

		} else if (!(parent instanceof CallableDeclaration)) {
			String nom = cd.getNameAsString();
			boolean setter = nom.length() > 3 && nom.startsWith("set") && Character.isUpperCase(nom.charAt(3));
			boolean getter = nom.length() > 3 && nom.startsWith("get") && Character.isUpperCase(nom.charAt(3));
			getter = getter || (nom.length() > 2 && nom.startsWith("is") && Character.isUpperCase(nom.charAt(2)));
			if (getter || setter) {
				erreurs.add(Erreur.GETTER_SETTER_SANS_JAVADOC.avecParametre(getter ? "getter" : "setter"));
			} else {
				erreurs.add(Erreur.ELEMENT_SANS_JAVADOC.avecParametre("méthode"));
			}
		}

		// Ajouter les erreurs de la méthode
		if (erreurs.size() > 0) {
			problemes.add(new Probleme(cd.getNameAsExpression(), erreurs));
		}
	}

	/**
	 * Vérifie si un identificateur est valide.
	 * 
	 * @param identificateur l'identificateur.
	 * @param constante      si l'identificateur est une constante.
	 * @return si l'identificateur est valide.
	 */
	private boolean identificateurValide(String identificateur, boolean constante) {
		if (constante) {
			return Pattern.matches("^[A-Z][A-Z_]*$", identificateur);
		} else {
			return Pattern.matches("^[a-zA-Z][a-zA-Z0-9]*$", identificateur);
		}
	}

	/**
	 * Vérifie si un nom de variable est valide.
	 * 
	 * @param identificateur l'identificateur.
	 * @return si l'identificateur est valide.
	 */
	private boolean nomVariableValide(String identificateur) {
		return identificateurValide(identificateur, false) && Character.isLowerCase(identificateur.charAt(0));
	}

	/**
	 * Vérifie si un nom de classe est valide.
	 * 
	 * @param identificateur l'identificateur.
	 * @return si l'identificateur est valide.
	 */
	private boolean nomClasseValide(String identificateur) {
		return identificateurValide(identificateur, false) && Character.isUpperCase(identificateur.charAt(0));
	}

	/**
	 * Vérifie si un nom de constante est valide.
	 * 
	 * @param identificateur l'identificateur.
	 * @return si l'identificateur est valide.
	 */
	private boolean nomConstanteValide(String identificateur) {
		return identificateurValide(identificateur, true);
	}

	/**
	 * Retourne le fichier vérifié.
	 * 
	 * @return le fichier vérifié.
	 */
	public File getFichier() {
		return fichier;
	}

	/**
	 * Retourne le code du fichier.
	 * 
	 * @return le code du fichier.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Retourne l'unité de compilation.
	 * 
	 * @return l'unité de compilation.
	 */
	public CompilationUnit getUniteCompilation() {
		return uniteCompilation;
	}

	/**
	 * Retourne les problèmes de documentation.
	 * 
	 * @return les problèmes de documentation.
	 */
	public Set<Probleme> getProblemes() {
		return problemes;
	}

	/**
	 * Retourne un code de hachage pour l'objet.
	 * 
	 * @return un code de hachage pour l'objet.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fichier == null) ? 0 : fichier.hashCode());
		return result;
	}

	/**
	 * Retourne vrai si cet objet est égal à un autre objet.
	 * 
	 * @param obj l'autre objet.
	 * @return vrai si cet objet est égal à l'autre objet.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Verification other = (Verification) obj;
		if (fichier == null) {
			if (other.fichier != null)
				return false;
		} else if (!fichier.equals(other.fichier))
			return false;
		return true;
	}

}
