/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package composants.visualisateur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JScrollPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;

import com.github.javaparser.Position;
import com.github.javaparser.Range;

import application.Theme;
import composants.InfoBulle;
import verification.Probleme;
import verification.Verification;

/**
 * Panneau permettant de visualiser du code Java.
 * 
 * @author Jean-Félix Morency
 */
public class VisualisateurJava extends JScrollPane {

	/** Identifiant de sérialisation. */
	private static final long serialVersionUID = 1L;

	/** La vérification à afficher. */
	private Verification verification;
	/** Le panneau de texte du code. */
	private TextPaneCode textPaneLignes;
	/** Le panneau de texte des numéros de ligne. */
	private TextPaneCode textPaneCode;
	/** L'info-bulle. */
	private InfoBulle infoBulle;

	/**
	 * Crée un panneau de visualisation du code.
	 */
	public VisualisateurJava() {
		super();
		setBorder(null);

		textPaneCode = new TextPaneCode();
		textPaneCode.setBorder(new CompoundBorder(textPaneCode.getBorder(), new EmptyBorder(0, 5, 0, 0)));
		Theme.ajouterComposant(textPaneCode);
		setViewportView(textPaneCode);

		textPaneLignes = new TextPaneCode();
		textPaneLignes.setEnabled(false);
		Theme.ajouterPanneau(textPaneLignes);
		setRowHeaderView(textPaneLignes);

		infoBulle = new InfoBulle();
		textPaneCode.add(infoBulle);

		textPaneCode.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				if (verification == null)
					return;

				Position position = textPaneCode.getPosition(e.getPoint());
				Probleme courant = infoBulle.getProbleme();

				if (courant != null && courant.getPlage().contains(position)) {
					infoBulle.setPosition(e.getX(), e.getY());
					infoBulle.setVisible(true);
					return;
				}

				courant = null;
				for (Probleme probleme : verification.getProblemes()) {
					if (probleme.getPlage().contains(position) && !probleme.getErreursActives().isEmpty()) {
						courant = probleme;
						break;
					}
				}

				if (courant != null) {
					infoBulle.setProbleme(courant);
					infoBulle.setPosition(e.getX(), e.getY());
					infoBulle.setVisible(true);
				} else {
					infoBulle.setVisible(false);
				}
			}
		});

		textPaneCode.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent arg0) {
				infoBulle.setVisible(false);
			}
		});

		Theme.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (verification != null) {
					textPaneCode.colorationSyntaxique(verification.getUniteCompilation());
					soulignerProblemes();
				}
			}
		});
	}

	/**
	 * Définit la position du curseur.
	 * 
	 * @param position la position du curseur.
	 */
	public void setCaretPosition(int position) {
		textPaneCode.setCaretPosition(position);
	}

	/**
	 * Retourne la position du curseur.
	 * 
	 * @return la position du curseur.
	 */
	public int getCaretPosition() {
		return textPaneCode.getCaretPosition();
	}

	/**
	 * Défile jusqu'à une ligne.
	 * 
	 * @param ligne la ligne.
	 */
	public void defilerALigne(int ligne) {
		textPaneCode.defilerALigne(ligne);
	}

	/**
	 * Modifie le fichier affiché.
	 * 
	 * @param verification le fichier affiché.
	 */
	public void setFichier(Verification verification) {
		this.verification = verification;
		if (verification == null) {
			textPaneCode.setText("");
			textPaneLignes.setText("");
			return;
		}

		textPaneCode.setCode(verification.getCode(), verification.getUniteCompilation());
		textPaneCode.setCaretPosition(0);

		String texteLignes = "";
		int longueurMax = Integer.toString(textPaneCode.getNbLignes()).length();
		for (int i = 1; i <= textPaneCode.getNbLignes(); i++) {
			String ligne = Integer.toString(i);
			texteLignes += " ".repeat(longueurMax - ligne.length()) + ligne + '\n';
		}
		textPaneLignes.setText(texteLignes);

		soulignerProblemes();
		surlignerLignes();
	}

	/**
	 * Retourne le fichier affiché.
	 * 
	 * @return le fichier affiché.
	 */
	public Verification getFichier() {
		return verification;
	}

	/**
	 * Actualise les erreurs affichées.
	 */
	public void actualiserErreurs() {
		if (verification == null)
			return;
		infoBulle.setProbleme(null);
		soulignerProblemes();
		surlignerLignes();
	}

	/**
	 * Souligne les problèmes dans le code.
	 */
	private void soulignerProblemes() {
		textPaneCode.getHighlighter().removeAllHighlights();
		for (Probleme probleme : verification.getProblemes()) {
			try {
				textPaneCode.souligner(probleme.getPlage(), Theme.getCouleurErreur(probleme.getSeverite()));
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Surligne les lignes qui ont des problèmes.
	 */
	private void surlignerLignes() {
		textPaneCode.effacerSurligneur();
		textPaneLignes.effacerSurligneur();
		for (Probleme probleme : verification.getProblemes()) {
			Range plage = probleme.getPlage();
			for (int ligne = plage.begin.line; ligne <= plage.end.line; ligne++) {
				textPaneLignes.surlignerLigne(ligne, probleme.getSeverite());
				textPaneCode.surlignerLigne(ligne, probleme.getSeverite());
			}
		}
	}

}
