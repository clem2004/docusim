/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package composants.visualisateur;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

import javax.swing.text.BadLocationException;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;

/**
 * Surligneur qui permet d'ajouter un soulignement ondulé au texte.
 * 
 * @author Jean-Félix Morency
 */
public class SurligneurOndule implements Highlighter.HighlightPainter {

	/** La couleur du surligneur. */
	private Color couleur;

	/**
	 * Crée un souligueur qui ajoute un soulignement oudulé au texte.
	 * 
	 * @param couleur la couleur du surligneur.
	 */
	public SurligneurOndule(Color couleur) {
		this.couleur = couleur;
	}

	/**
	 * Surligne du texte.
	 * 
	 * @param g              le contexte graphique.
	 * @param debut          l'indice du premier caractère souligné.
	 * @param fin            l'indice du dernier caractère souligné.
	 * @param dimensions     les dimensions maximale du surlignage.
	 * @param composantTexte le composant textuel.
	 */
	@Override
	public void paint(Graphics g, int debut, int fin, Shape dimensions, JTextComponent composantTexte) {
		try {

			String texte = composantTexte.getText(debut, fin - debut + 1);

			int position = 0;
			do {
				if (Character.isWhitespace(texte.charAt(position))) {
					position++;
					continue;
				}

				int finLigne = texte.indexOf('\n', position);
				if (finLigne == -1 || finLigne > texte.length()) {
					finLigne = texte.length();
				}

				Rectangle2D rect = composantTexte.modelToView2D(debut + position);
				int xInitial = (int) rect.getX();
				int xFinal = xInitial + g.getFontMetrics().stringWidth(texte.substring(position, finLigne));
				int y = (int) (rect.getY() + rect.getHeight());

				g.setColor(couleur);
				boolean enHaut = false;
				for (int x = xInitial; x < xFinal; x += 2) {
					if (enHaut) {
						g.drawLine(x, y - 2, x + 2, y);
					} else {
						g.drawLine(x, y, x + 2, y - 2);
					}
					enHaut = !enHaut;
				}

				position = finLigne + 1;
			} while (position < texte.length());

		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

}
