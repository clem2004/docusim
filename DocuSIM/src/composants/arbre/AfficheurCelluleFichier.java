/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package composants.arbre;

import java.awt.Component;
import java.awt.Font;
import java.util.HashSet;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import application.Theme;
import verification.Severite;
import verification.Verification;

/**
 * Afficheur de cellule pour l'arbre des fichiers.
 * 
 * @author Jean-Félix Morency
 */
public class AfficheurCelluleFichier extends AfficheurCellule {

	/** Identifiant de sérialisation. */
	private static final long serialVersionUID = 1L;

	/**
	 * Retourne le composant à afficher pour une cellule.
	 * 
	 * @param tree     l'arbre.
	 * @param value    la cellule.
	 * @param sel      si la cellule est sélectionnée.
	 * @param expanded si la cellule est étendue.
	 * @param leaf     si la cellule est une feuille.
	 * @param row      le numéro de rangée de la cellule.
	 * @param hasFocus si la cellule a le focus.
	 * @return le composant à afficher pour une cellule.
	 */
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
			int row, boolean hasFocus) {

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if (node.getUserObject() instanceof Verification) {
			Verification verif = (Verification) node.getUserObject();
			value = verif.getFichier().getName();
		}
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

		Severite severite = severiteNoeud(node);
		if (severite == null) {
			setFont(new Font(getFont().getName(), Font.PLAIN, getFont().getSize()));
		} else {
			setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
			setForeground(Theme.getCouleurErreur(severite));
		}

		return this;
	}

	/**
	 * Détermine la plus grande sévérité des erreurs dans un noeud.
	 * 
	 * @param node le noeud.
	 * @return la plus grande sévérité des erreurs dans le noeud.
	 */
	private Severite severiteNoeud(DefaultMutableTreeNode node) {
		HashSet<Severite> severites = new HashSet<>();
		if (node.getUserObject() instanceof Verification) {
			Severite severite = ((Verification) node.getUserObject()).getSeverite();
			if (severite != null)
				severites.add(severite);
		}
		for (int i = 0; i < node.getChildCount(); i++) {
			Severite severite = severiteNoeud((DefaultMutableTreeNode) node.getChildAt(i));
			if (severite != null)
				severites.add(severite);
		}
		if (severites.size() == 0) {
			return null;
		}
		return Severite.maximale(severites);
	}
}
