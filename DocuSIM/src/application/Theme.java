/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package application;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.Preferences;

import verification.ElementSyntaxique;
import verification.Severite;

/**
 * Classe contenant les couleurs à utiliser pour l'application.
 * 
 * @author Jean-Félix Morency
 */
public class Theme {

	/** Les préférences de l'utilisateur. */
	private static Preferences preferences = Preferences.userRoot().node("docusim");
	/** Si le thème sombre est actif. */
	private static boolean sombre = preferences.getBoolean("sombre", false);
	/** Les écouteurs d'évènement. */
	private static List<ActionListener> ecouteurs = new LinkedList<>();

	/**
	 * Ajoute un écouteur d'évènement qui s'active lorsque le thème change.
	 * 
	 * @param ecouteur l'écouteur d'évènement.
	 */
	public static void addActionListener(ActionListener ecouteur) {
		ecouteurs.add(ecouteur);
	}

	/**
	 * Ajoute un composant à garder synchronisé avec le thème.
	 * 
	 * @param composant le composant.
	 */
	public static void ajouterComposant(Component composant) {
		composant.setForeground(getCouleurTexte());
		composant.setBackground(getCouleurFond());
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				composant.setForeground(getCouleurTexte());
				composant.setBackground(getCouleurFond());
			}
		});
	}

	/**
	 * Ajoute un panneau à garder synchronisé avec le thème.
	 * 
	 * @param panneau le panneau.
	 */
	public static void ajouterPanneau(Component panneau) {
		panneau.setForeground(getCouleurTexte());
		panneau.setBackground(getCouleurPanneau());
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panneau.setForeground(getCouleurTexte());
				panneau.setBackground(getCouleurPanneau());
			}
		});
	}

	/**
	 * Modifie le thème utilisé.
	 * 
	 * @param sombre si le thème sombre est actif.
	 */
	public static void setSombre(boolean sombre) {
		Theme.sombre = sombre;
		preferences.putBoolean("sombre", sombre);
		ecouteurs.forEach(ecouteur -> ecouteur.actionPerformed(null));
	}

	/**
	 * Retourne vrai si le thème sombre est actif.
	 * 
	 * @return vrai si le thème sombre est actif.
	 */
	public static boolean isSombre() {
		return sombre;
	}

	/**
	 * Retourne la couleur d'un élément syntaxique.
	 * 
	 * @param element le type d'élément syntaxique.
	 * @return la couleur de l'élément syntaxique.
	 */
	public static Color getCouleurElementSyntaxique(ElementSyntaxique element) {
		switch (element) {
		case MOT_CLE:
			return new Color(sombre ? 0xCC6C1D : 0x7F0055);
		case COMMENTAIRE:
			return new Color(sombre ? 0x808080 : 0x3F7F5F);
		case JAVADOC:
			return new Color(sombre ? 0x808080 : 0x3F5FBF);
		case CLASSE:
			return new Color(sombre ? 0x1290C3 : 0);
		case ANNOTATION:
			return new Color(sombre ? 0x808080 : 0x646464);
		case METHODE:
			return new Color(sombre ? 0xA7EC21 : 0);
		case PROPRIETE:
			return new Color(sombre ? 0x66E1F8 : 0x0000C0);
		case PARAMETRE:
			return new Color(sombre ? 0x79ABFF : 0);
		case VARIABLE:
			return new Color(sombre ? 0xF3EC79 : 0x6A3E3E);
		case NOMBRE:
			return new Color(sombre ? 0x6897BB : 0);
		case CHAINE:
			return new Color(sombre ? 0x17C6A3 : 0x6A3E3E);
		default:
			throw new NullPointerException();
		}
	}

	/**
	 * Retourne la couleur du texte.
	 * 
	 * @return la couleur du texte.
	 */
	public static Color getCouleurTexte() {
		return sombre ? Color.WHITE : Color.BLACK;
	}

	/**
	 * Retourne la couleur de fond.
	 * 
	 * @return la couleur de fond.
	 */
	public static Color getCouleurFond() {
		return new Color(sombre ? 0x2F2F2F : 0xFFFFFF);
	}

	/**
	 * Retourne la couleur de fond des panneaux.
	 * 
	 * @return la couleur de fond des panneaux.
	 */
	public static Color getCouleurPanneau() {
		return new Color(sombre ? 0x34393D : 0xF0F0F0);
	}

	/**
	 * Retourne la couleur d'une erreur.
	 * 
	 * @param severite la sévérité de l'erreur.
	 * @return la couleur de l'erreur.
	 */
	public static Color getCouleurErreur(Severite severite) {
		if (severite == null)
			return new Color(0, 0, 0, 0);

		switch (severite) {
		case ERREUR_JAVA:
			return sombre ? new Color(255, 50, 50) : Color.RED;

		case NORME_SIM:
			return new Color(sombre ? 0x8A4CFF : 0xAB44FF);

		case AVERTISSEMENT:
			return sombre ? Color.YELLOW : Color.ORANGE.darker();

		default:
			throw new NullPointerException();
		}
	}

	/**
	 * Retourne la couleur de surligneur pour une erreur.
	 * 
	 * @param severite la sévérité de l'erreur.
	 * @return la couleur de surligneur pour l'erreur.
	 */
	public static Color getCouleurSurligneur(Severite severite) {
		if (severite == null)
			return new Color(0, 0, 0, 0);

		switch (severite) {
		case ERREUR_JAVA:
			return sombre ? new Color(187, 9, 9, 60) : new Color(255, 114, 114, 60);

		case NORME_SIM:
			return sombre ? new Color(136, 9, 186, 60) : new Color(208, 89, 255, 60);

		case AVERTISSEMENT:
			return sombre ? new Color(187, 128, 9, 60) : new Color(255, 233, 114, 102);

		default:
			throw new NullPointerException();
		}
	}

}
